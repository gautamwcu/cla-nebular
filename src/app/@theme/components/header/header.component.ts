import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';

import { filter, map, takeUntil } from 'rxjs/operators';
import { Subscription, Subject } from 'rxjs';
import * as $ from 'jquery';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

  private destroy$: Subject<void> = new Subject<void>();

  @Input() position = 'normal';
  // private routeSubscription: Subscription = null;
  // private profileImageSubscription: Subscription = null;
  // public user: LoggedInUserDetails = null;
  public profilePic: string = '';
  THEME: string = '';
  userMenu = [{ title: 'Profile', link: '/pages/user-profile' },
  { title: 'Log out', link: '/logout' }];
  themes = [
    {
      value: 'corporate',
      name: 'Default',
      icon: 'fa-sun'
    },
    {
      value: 'dark',
      name: 'Dark',
      icon: 'fa-moon'
    },
    {
      value: 'default',
      name: 'Compact',
      icon: 'fa-compress'
    },
    {
      value: 'cosmic',
      name: 'Dark Compact',
      icon: 'fa-compress-arrows-alt'
    }
  ];
  currentTheme: any = {};

  constructor(private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
   // private analyticsService: AnalyticsService,
    //private layoutService: LayoutService,
    private route: ActivatedRoute,
    private router: Router,
    //private graphService: GraphService,
    //public sharedService: SharedService,
    private themeService: NbThemeService) {

    this.currentTheme = this.themes[0];
    // let theme = JSON.parse(localStorage.getItem("current-selected-theme"));
    // if (theme && this.currentTheme.value !== theme.value) {
    //   this.currentTheme = theme;
    //   this.changeTheme(this.currentTheme);
    // }

    // this.routeSubscription = this.route.params.subscribe(params => {
    //   this.sharedService.THEME = THEME.CLA;
    //   if (params['app'] && THEME.WCU === params['app'].toUpperCase()) {
    //     this.sharedService.THEME = params['app'];
    //   }
    // });
    // this.profileImageSubscription = this.sharedService.profileImageChanged.subscribe(value => {
    //   this.initializeUser();
    // });
  }

  ngOnInit() {
    this.currentTheme = this.themes.filter(x => x.value === this.themeService.currentTheme)[0];
    this.initializeUser();
    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => this.currentTheme = this.themes.filter(x => x.value === themeName)[0]);
  }

  initializeUser() {
    // this.user = JSON.parse(sessionStorage.getItem('LoggedInUser'));
    // this.displayProfilePic();
    // if (this.user) {
    //   this.user.UserFullName = this.user.FirstName + ' ' + this.user.LastName;
    // }
  }

  toggleTheme() {
    let idx = this.themes.indexOf(this.currentTheme);
    idx = (idx + 1) % 4;
    this.changeTheme(this.themes[idx]);
  }

  changeTheme(themeName: any) {
    this.currentTheme = themeName;
    localStorage.setItem("current-selected-theme", JSON.stringify(themeName));
    this.themeService.changeTheme(themeName.value);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(false, 'menu-sidebar');
    //this.layoutService.changeLayoutSize();

    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  goToHomePage() {
    this.router.navigate(['/pages']);
  }

  startSearch() {
    //this.analyticsService.trackEvent('startSearch');
  }

  displayProfilePic() {
    // if (this.user && this.user.ProfilePhoto) {
    //   const byteArr = this.sharedService.getByteArrayFromImageContent(this.user.ProfilePhoto);
    //   this.profilePic = 'data:image/png;base64,' +
    //    this.sharedService.encode(byteArr);
    // }
    // this.graphService.getMyProfilePic()
    //   .then((result) => {
    //     if (result) {
    //       try {
    //         // let base64String = btoa(String.fromCharCode.apply(null, new Uint8Array(result)));
    //         let base64String = btoa(new Uint8Array(result).reduce(function (data, byte) {
    //           return data + String.fromCharCode(byte);
    //         }, ''));
    //         this.profilePic = 'data:image/png;base64,' + base64String;
    //       } catch (e) {
    //         console.log(e)
    //       }
    //     }
    //   });
  }

  ngOnDestroy(): void {
    // this.profileImageSubscription.unsubscribe();
    // this.routeSubscription.unsubscribe();
    this.destroy$.next();
    this.destroy$.complete();
  }

  isMobileMenu() {
    // if ($(window).width() > 991) {
    //   return false;
    // }
    return true;
  }
}
