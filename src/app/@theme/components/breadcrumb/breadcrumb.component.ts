import { Component, Input, OnDestroy } from '@angular/core';
//import { MENU_ITEMS } from '../../../pages/pages-menu';
import { NbMenuItem } from '@nebular/theme';
import { Router, NavigationEnd, NavigationStart, ActivatedRoute } from '@angular/router';
//import { SharedService } from '../../../@services/shared.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ngx-breadcrumb',
  styleUrls: ['./breadcrumb.component.scss'],
  templateUrl: 'breadcrumb.component.html'
})
export class BreadcrumbComponent implements OnDestroy {
  @Input() value: any;
  breadcrumbArr: Array<any> = [];
  lastURLLoaded: string = '';
 // private routerSubscription: Subscription = null;
  //private menuListingSubscription: Subscription = null;

  constructor(private router: Router, 
    //private sharedService: SharedService, 
    private activeRoute: ActivatedRoute) {
    this.lastURLLoaded = window.location.hash.replace("#","");
    // this.routerSubscription = router.events.subscribe((event) => {
    //   if (event instanceof NavigationStart) {
    //   }

    //   if (event instanceof NavigationEnd) {
    //     this.lastURLLoaded = event.url;
    //     this.getBreadcrumb();
    //   }
    // });

    // this.menuListingSubscription = this.sharedService.menuListingChanged.subscribe(event => {
    //   this.getBreadcrumb();
    // });
  }

  getBreadcrumb() {
    this.breadcrumbArr = [];
    //this.changeValue(MENU_ITEMS, this.lastURLLoaded);
  }

  changeValue(menus: NbMenuItem[], link: string) {
    var url = this.lastURLLoaded.split('/');
    if (url[1] === 'pages' && url[2] === 'student-details') {
      
      this.breadcrumbArr.splice(this.breadcrumbArr.length - 1, 1);
      var breadcrumb0 = { title: 'SAP Appeal' };
      var breadcrumb1 = { title: 'Student Details' };
      var breadcrumb2 = { title: decodeURI(url[3]) };
      this.breadcrumbArr.push(breadcrumb0, breadcrumb1, breadcrumb2);
      return true;
    }
    else if (url[1] === 'pages' && url[2] === 'search-students') {
      
      this.breadcrumbArr.splice(this.breadcrumbArr.length - 1, 1);
      var breadcrumb0 = { title: 'Proposed Schedule' };
      var breadcrumb1 = { title: 'Search Students' };
      this.breadcrumbArr.push(breadcrumb0, breadcrumb1);
      return true;
    }
    else if (url[1] === 'pages' && url[2] === 'course-management') {
      
      this.breadcrumbArr.splice(this.breadcrumbArr.length - 1, 1);
      var breadcrumb0 = { title: 'Proposed Schedule' };
      var breadcrumb1 = { title: 'Course Management' };
      this.breadcrumbArr.push(breadcrumb0, breadcrumb1);
      return true;
    }
    for (let i = 0; i < menus.length; i++) {
      const v = menus[i];
      this.breadcrumbArr.push(v);
      // if (v.link === link) {
      //   const oacID = this.sharedService.getPersistedOACId();
      //   if (this.lastURLLoaded === '/pages/review-upload-documents' && oacID) {
      //     this.breadcrumbArr.push({
      //       title: oacID
      //     });
      //   }
      //   return true;
      // } else if (link.startsWith(v.link)) {
      //   const urlStringSplitted = link.split('/');
      //   const routeStringSplitted = v.link.split('/');
      //   for (let j = routeStringSplitted.length; j < urlStringSplitted.length; j++) {
      //     this.breadcrumbArr.push({
      //       title: urlStringSplitted[j]
      //     });
      //   }
      //   return true;
      // } else if (v.children && v.children.length > 0) {
      //   if (this.changeValue(menus[i].children, link)) {
      //     return true;
      //   } else {
      //     this.breadcrumbArr.splice(this.breadcrumbArr.length - 1, 1);
      //   }
      // } else {
      //   this.breadcrumbArr.splice(this.breadcrumbArr.length - 1, 1);
      // }
    }
    
    return false;
    //this.valueChange.emit(this.value);
  }

  ngOnDestroy(): void {
   // this.routerSubscription.unsubscribe();
    //this.menuListingSubscription.unsubscribe();
  }
}
