import { Component, OnDestroy, AfterViewInit, OnInit } from '@angular/core';
import { delay, withLatestFrom, takeWhile } from 'rxjs/operators';
import {
  NbMediaBreakpoint,
  NbMediaBreakpointsService,
  NbMenuItem,
  NbMenuService,
  NbSidebarService,
  NbThemeService,
} from '@nebular/theme';

//import { StateService } from '../../../@core/utils';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

// TODO: move layouts into the framework
@Component({
  selector: 'ngx-sample-layout',
  styleUrls: ['./sample.layout.scss'],
  template: `
    <nb-layout id="layout-container" [center]="layout.id === 'center-column'" windowMode>


      <nb-layout-header fixed *ngIf='isVisible'>
        <ngx-header [position]="sidebar.id === 'start' ? 'normal': 'inverse'"></ngx-header>
      </nb-layout-header>

      <nb-sidebar *ngIf='isVisible' class="menu-sidebar"
                   tag="menu-sidebar"
                   state="collapsed"
                   responsive
                   [end]="sidebar.id === 'end'">
        <ng-content select="nb-menu"></ng-content>
      </nb-sidebar>

      <nb-layout-column id="main-container" class="main-content">
        <ng-content select="router-outlet"></ng-content>
      </nb-layout-column>
    </nb-layout>
  `,
})
export class SampleLayoutComponent implements OnDestroy, OnInit {
  layout: any = {};
  sidebar: any = {};
  lockSidebar: boolean = true;
  isVisible:boolean = true;

  private alive = true;

  //currentTheme: string;
  // private routerSubscription: Subscription = null;
  // private layoutSubscription: Subscription = null;
  // private sidebarSubscription: Subscription = null;
  // private menuSubscription: Subscription = null;
  // private jsThemeSubscription: Subscription = null;
 

  constructor(
    //protected stateService: StateService,
    protected menuService: NbMenuService,
    protected themeService: NbThemeService,
    protected bpService: NbMediaBreakpointsService,
    protected sidebarService: NbSidebarService,
    private router: Router) {

    // this.routerSubscription = this.router.events.subscribe((val) => {
    //   if (val instanceof NavigationEnd) {
    //     if (val.url.toLowerCase() === '/pages' || val.url.toLowerCase() === '/acc/pages' || val.url.toLowerCase() === '/wcu/pages') {
    //       this.lockSidebar = true;
    //     }
    //     if (val.url.indexOf("student360") > -1){
    //       if((val.url.split("/").length==5 && val.url.split("/")[4]=="1") || (val.url.split("/").length==4 && val.url.split("/")[3]=="1") )
    //       {
    //         this.isVisible = false;
    //       }else{
    //         this.isVisible = true;
    //       }
    //     }
    //     else if (val.url.indexOf("start-conversation") > -1) {
    //       if ((val.url.split("/").length == 5 && (val.url.split("/")[4] == "1" || val.url.split("/")[4] == "2")) || (val.url.split("/").length == 4 && (val.url.split("/")[3] == "1" || val.url.split("/")[3] == "2"))) {
    //         this.isVisible = false;
    //       } else {
    //         this.isVisible = true;
    //       }
    //     }
    //     else if (val.url.indexOf("inbox") > -1) {
    //       if (val.url.split("/").length == 4 && val.url.split("/")[3] == "1") {
    //         this.isVisible = false;
    //       } else {
    //         this.isVisible = true;
    //       }
    //     }
    //   }
    // });

  //   this.routeSubscription = this.route.params.subscribe(params => {
  //     let adEnrollId = params["adEnrollId"];
  //     let isIframe = params["isIframe"];
  //     if(isIframe == "1")
  //     {
  //       this.isVisible = false;
  //     }
  //  });


    // this.layoutSubscription = this.stateService.onLayoutState()
    //   .pipe(takeWhile(() => this.alive))
    //   .subscribe((layout: string) => this.layout = layout);

    // this.sidebarSubscription = this.stateService.onSidebarState()
    //   .pipe(takeWhile(() => this.alive))
    //   .subscribe((sidebar: string) => {
    //     this.sidebar = sidebar;
    //   });

    const isBp = this.bpService.getByName('is');

    // this.menuSubscription = this.menuService.onItemSelect()
    //   .pipe(
    //     takeWhile(() => this.alive),
    //     withLatestFrom(this.themeService.onMediaQueryChange()),
    //     delay(20),
    //   )
    //   .subscribe(([item, [bpFrom, bpTo]]: [any, [NbMediaBreakpoint, NbMediaBreakpoint]]) => {

    //     if (item.item && item.item.title === 'MENU') {
    //       this.sidebarService.expand('menu-sidebar');
    //       this.lockSidebar = true;
    //     } else {
    //       this.lockSidebar = false;
    //     }

    //     if (bpTo.width <= isBp.width) {
    //       this.sidebarService.collapse('menu-sidebar');
    //     }
    //   });

    // this.jsThemeSubscription = this.themeService.getJsTheme()
    //   .pipe(takeWhile(() => this.alive))
    //   .subscribe(theme => {
    //     this.currentTheme = theme.name;
    //   });
  }

  ngOnInit() {
    if (this.lockSidebar) {
      this.sidebarService.expand('menu-sidebar');
    }
  }

  // ngAfterViewInit() {
  //   if (this.lockSidebar) {
  //     this.sidebarService.expand('menu-sidebar');
  //   }
  // }

  ngOnDestroy() {
    this.alive = false;
   // this.routerSubscription.unsubscribe();
    // this.layoutSubscription.unsubscribe();
    // this.sidebarSubscription.unsubscribe();
    // this.menuSubscription.unsubscribe();
    // this.jsThemeSubscription.unsubscribe();
  }

  mouseover() {
    this.sidebarService.expand('menu-sidebar');
  }

  mouseout() {
    if (!this.lockSidebar) {
      this.sidebarService.toggle(false, 'menu-sidebar');
    }
  }

}
