import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "filter" })
export class UserFilterPipe implements PipeTransform {
  public transform(item: any, inputVal) {
    return item.filter((e) => e.UserFullName.toLowerCase().startsWith(inputVal.toLowerCase()));
  }
}
