import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'initials',
})
export class InitialsPipe implements PipeTransform {
  transform(value: string) {
    if (value) {
      return value.substring(0, 1).toLocaleUpperCase();
    }
    return value;
  }
}
