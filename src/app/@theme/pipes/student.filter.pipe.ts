import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'studentFilter' })
export class StudentFilterPipe implements PipeTransform {
  public transform(item: any, inputVal) {
    const filteredItem: Array<any> = item.filter((e) =>
     e.FullName.toLowerCase().includes(inputVal.toLowerCase()));
    return (filteredItem.length > 10) ? filteredItem.slice(0, 10) : filteredItem;
  }
}
