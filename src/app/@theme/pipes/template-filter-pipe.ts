import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "filterT" })
export class TemplateFilterPipe implements PipeTransform {
  public transform(item: any, inputVal) {
    return item.filter((e) => e.TemplateName.toLowerCase().startsWith(inputVal.toLowerCase()));
  }
}
