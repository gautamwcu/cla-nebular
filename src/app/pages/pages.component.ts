import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef, NgZone } from '@angular/core';
//import { MENU_ITEMS } from './pages-menu';
//import { SharedService } from '../@services/shared.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { NbIconLibraries, NbMenuItem, NbSidebarService } from '@nebular/theme';
//import { THEME } from '../@models';
// import {
//   LoggedInUserDetails
// } from '../auth/auth.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
  template: `
  <ngx-sample-layout>
    <nb-menu [items]="menu"></nb-menu>
    <router-outlet></router-outlet>
  </ngx-sample-layout>
  `
})
export class PagesComponent implements OnInit, OnDestroy {
  private routerSubscription: Subscription = new Subscription();
  private eventsSubscription: Subscription = new Subscription();
  private SaveUserRecentMenuSubscription: Subscription = new Subscription();
  isAuthenticated: boolean = false;
  //public loggedInUserDetails: LoggedInUserDetails = null;
  menu = [];

  constructor(
      //private sharedService: SharedService,
    private route: ActivatedRoute,
    private sideNavService: NbSidebarService,
    private ref: ChangeDetectorRef,
    private iconLibraries: NbIconLibraries,
    private router: Router) {
    this.routerSubscription = this.route.params.subscribe(params => {
    //   this.sharedService.THEME = THEME.ACC;
    //   if (params['app'] && THEME.WCU === params['app'].toUpperCase()) {
    //     this.sharedService.THEME = params['app'];
    //     this.ref.detectChanges();
    //   }
    });

    this.iconLibraries.registerFontPack('solid', {packClass: 'fas', iconClassPrefix: 'fa'});
    this.iconLibraries.registerFontPack('regular', {packClass: 'far', iconClassPrefix: 'fa'});
    this.iconLibraries.registerFontPack('light', {packClass: 'fal', iconClassPrefix: 'fa'});
    this.iconLibraries.registerFontPack('duotone', {packClass: 'fad', iconClassPrefix: 'fa'});
    this.iconLibraries.registerFontPack('brands', {packClass: 'fab', iconClassPrefix: 'fa'});
   // this.iconLibraries.registerFontPack('font-awesome', {packClass: 'fab', iconClassPrefix: 'fa'});
    this.iconLibraries.registerFontPack('font-awesome', { iconClassPrefix: 'fa' });
    this.iconLibraries.setDefaultPack('font-awesome');
    //this.iconLibraries.setDefaultPack('light');
  }

  ngOnInit() {
    // this.getMenuIfAuthenticated();
    // this.sharedService.menuListener.subscribe((menuList = MENU_ITEMS) => {
    //   this.menu = menuList;
    //   if(this.menu &&  this.menu.length>0){
    //     this.menu.forEach(x=>{

    //       let iconObj:any = {
    //         icon:x.icon,
    //         pack:'font-awesome'
    //       }
    //       x.icon = iconObj;

    //     });
    //   }
    //   const loggedInUserDetails = JSON.parse(sessionStorage.getItem('LoggedInUser'));
    //   if (loggedInUserDetails) {
    //     let email = loggedInUserDetails.Email ? loggedInUserDetails.Email.toLowerCase() : '';
    //     if (email.indexOf('u.americancareercollege.edu') >= 0 || email.indexOf('u.westcoastuniversity.edu') >= 0) {
    //       this.router.navigate(["/pages/401"]);
    //     }
    //   }
    //   this.addLink(menuList);
    //   this.setMenuRecent(menuList);
    //   this.sharedService.menuListingChanged.next(true);
    //   this.ref.detectChanges();
    // });

    this.menu = [
      {
        title: 'OAC',
        icon:{
          icon: 'university',
          pack:'font-awesome'
        }
       
      },
        {
          title: 'Profile',
          icon: 'person-outline',
        },
        {
          title: 'Change Password',
          icon: 'lock-outline',
        },
        {
          title: 'Privacy Policy',
          icon: { icon: 'checkmark-outline', pack: 'eva' },
        },
        {
          title: 'Logout',
          icon: 'unlock-outline',
        },
      ];
    this.ref.detectChanges();
  }

  // getMenuIfAuthenticated(menuList?: any) {
  //   this.isAuthenticated = this.sharedService.IsAuthenticated();
  //   if (this.isAuthenticated) {
  //     const menu = (menuList && menuList.ListOfPermissionWithSuperGroup.length > 0) ? menuList : this.route.snapshot.data.menu;
  //     if (menu && menu.ListOfPermissionWithSuperGroup.length > 0) {
  //       this.menu.splice(0, 1);
  //       menu.ListOfPermissionWithSuperGroup.forEach(element => {
  //         // below code is temporary to hide icon errors from console
  //         delete element.icon;
  //         if (element.children && element.children.length > 0) {
  //           element.children.forEach((item) => {
  //             delete item.icon;
  //           });
  //         }
  //         // ----------------- icon hide code above ----------------
  //         // start remove specific menu
  //         element.children.map((res, i) => {
  //           if (res.title === 'Registrar Email') {
  //             element.children.splice(i, 1);
  //           }
  //         });
  //         // end remove specific menu
  //         this.menu.push(element);
  //       });
  //       this.addLink(this.menu);
  //       this.ref.detectChanges();
  //       this.setMenuRecent(this.menu);
  //       this.ref.detectChanges();
  //       this.sharedService.menuListingChanged.next(true);
  //       this.ref.detectChanges();
  //     } else {
  //       this.getMenuIfIdWasNotFoundAtInitialLoad();
  //     }
  //   }
  // }

  // getMenuIfIdWasNotFoundAtInitialLoad() {
  //   const loggedInUserDetails = JSON.parse(sessionStorage.getItem('LoggedInUser'));
  //   this.GetUserAssignedPermissionGroupsForMenuSubscription =
  //     this.sharedService.GetUserAssignedPermissionGroupsForMenu(loggedInUserDetails.UserId)
  //       .subscribe(x => this.getMenuIfAuthenticated(x))
  //   this.ref.detectChanges();
  // }

  addLink(menus: NbMenuItem[]) {
    if (menus && menus.length > 0) {
      menus.forEach(m => {
        if (m.link) {
          m.link = m.link;
        }
       // this.addLink(m.children);
      });
    }
  }

//   setMenuRecent(menus: NbMenuItem[]) {
//     this.eventsSubscription = this.router.events.subscribe((val) => {
//       let menuList: NbMenuItem[] = [];
//       if (val instanceof NavigationEnd) {
//         menus.forEach(element => {
//          // menuList = menuList.concat(element.children);
//         });

//         let currentMenu = menuList.filter(x => x && x.link && val.url==x.link)[0];
//         if(!currentMenu){
//           currentMenu = menuList.filter(x => x && x.link && val.url.indexOf(x.link) > -1)[0];
//         }

//         if (currentMenu && currentMenu.data && currentMenu.data.Id) {
//           //this.loggedInUserDetails = JSON.parse(sessionStorage.getItem('LoggedInUser'));
//           this.SaveUserRecentMenuSubscription = this.sharedService.SaveUserRecentMenu({
//             PermissionGroupMasterId: currentMenu.data.Id,
//             //UserId: this.loggedInUserDetails.UserId
//           }).subscribe(x => {
//            // this.sharedService.logger(x);
//           }, err => { })
//         }
//       }
//     });
//   }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.routerSubscription.unsubscribe();
    this.eventsSubscription.unsubscribe();
    this.SaveUserRecentMenuSubscription.unsubscribe();
  }
}
