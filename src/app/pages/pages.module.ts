import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { NbMenuModule } from '@nebular/theme';

const PAGES_COMPONENTS = [
  PagesComponent
];

@NgModule({
  imports: [
    PagesRoutingModule,
    NbMenuModule,
    ThemeModule
  ],
  declarations: [
    ...PAGES_COMPONENTS
  ],
  providers: [],
  entryComponents: []
})
export class PagesModule {
}
