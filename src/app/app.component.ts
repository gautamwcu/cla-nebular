import { Component, OnDestroy, OnInit } from '@angular/core';
@Component({
  selector: 'ngx-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'cla-portal';
  ngOnInit(): void {
    console.log("app component started")
  }
  ngOnDestroy(): void {
    
    console.log("app component destroyed")
  }
}
